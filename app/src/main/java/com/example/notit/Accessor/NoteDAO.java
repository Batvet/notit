package com.example.notit.Accessor;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.notit.Model.Note;

import java.util.List;

@Dao
public interface NoteDAO {
    @Insert
    void insertNotes(Note... notes);

    @Delete
    void deleteNotes(Note... notes);

    @Update
    void update(Note... notes);

//    @Query("UPDATE note SET title = :title ,description = :description,date= :date WHERE uid LIKE :id ")
//    int updateItem(int id,String title,String description,String date);

    @Query("SELECT * FROM note")
    List<Note> getAllNotes();
}
