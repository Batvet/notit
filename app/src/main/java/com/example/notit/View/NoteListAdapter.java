package com.example.notit.View;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.notit.Model.Note;
import com.example.notit.R;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private List<Note> noteList;
    private NoteViewHolder noteViewHolder;


    public NoteListAdapter(List<Note> noteList) {
        this.noteList = noteList;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_note_display, parent, false);
        noteViewHolder = new NoteViewHolder(itemView);

        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final NoteViewHolder holder, int i) {
        Note note = noteList.get(i);
        holder.titreView.setText(note.getTitre());
        holder.dateView.setText(note.getDate());
        holder.descriptionView.setText(note.getDescription());

        //Get index of clicked note
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent switchActivity = new Intent(holder.itemView.getContext(), AddEditNoteActivity.class);

                Note n = new Note();
                n.setUid(holder.getAdapterPosition());
                n.setTitre(holder.titreView.getText().toString());
                n.setDescription(holder.descriptionView.getText().toString());

                switchActivity.putExtra("note", n);

                holder.itemView.getContext().startActivity(switchActivity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void restoreItem(Note note, int position) {
        noteList.add(position, note);
        // notify item added by position
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        noteList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

}
