package com.example.notit.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.notit.Accessor.AppDatabase;
import com.example.notit.Accessor.NoteDAO;
import com.example.notit.MainActivity;
import com.example.notit.Model.Note;
import com.example.notit.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;
public class AddEditNoteActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {
    Button addNote;

    private AppDatabase appdb;
    private NoteDAO noteDAO;

    Note note;

    String titleGet, descriptionGet;
    EditText titleEditText, descriptionEditText;

    GoogleMap myMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        setUpActionBar();

        appdb = AppDatabase.getDatabase(this);
        noteDAO = appdb.noteDAO();

        titleEditText = (EditText) findViewById(R.id.text_edit_note_titre);
        descriptionEditText = (EditText) findViewById(R.id.text_edit_note_description);
        addNote = (Button) findViewById(R.id.valider);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.myMap);
        mapFragment.getMapAsync(this);

        displayNote();

        if(note != null) {
            modifyNoteInDatabse();
        }
        else addNotesToDatabase();

        setUpMap();
    }

    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add note");
        actionBar.setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
        /* actionBar.setIcon(R.drawable.ic_add_note_white_24dp);*/
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    //TODO: Resolved modified values in database & view
    private void addNotesToDatabase() {

        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = String.valueOf(((EditText) findViewById(R.id.text_edit_note_titre)).getText());
                String description = String.valueOf(((EditText) findViewById(R.id.text_edit_note_description)).getText());
                String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

                Note note = new Note(
                        title,
                        description,
                        date,
                        0,
                        0
                );

                if (!title.equals("") && !description.equals("")) {

                    //TODO: When a note is modified, in the View, we see 2 same notes (it replaces the note above the note modified). The modifications are never saved
                    (new AddEditNoteActivity.InsertAsyncTask(noteDAO)).execute(note);

                    Intent switchActivity = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("status", "Note added");
                    switchActivity.putExtras(extras);
                    startActivity(switchActivity);
                } else {
                    Snackbar snackbar = Snackbar.make(view, "Title & description need to be furfilled", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });
    }

    private void displayNote() {
        getNote();

        titleEditText.setText(titleGet);
        descriptionEditText.setText(descriptionGet);
    }

    //Get index of a clicked Note
    private void getNote() {
        Intent switchActivity = getIntent();

        if (switchActivity != null && switchActivity.getExtras() != null) {
            note = (Note) switchActivity.getSerializableExtra("note");
            titleGet = note.getTitre();
            descriptionGet = note.getDescription();

            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "uid note modified "+note.getUid(), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void modifyNoteInDatabse() {

        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                note.setDate(date);

                if (!titleGet.equals("") && !descriptionGet.equals("")) {

                    (new UpdateAsyncTask(noteDAO)).execute(note);

                    Intent switchActivity = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("status", "Note modified");
                    switchActivity.putExtras(extras);
                    startActivity(switchActivity);
                } else {
                    Snackbar snackbar = Snackbar.make(view, "Title & description can\'t be empty", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });
    }

    private static class UpdateAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO dao;

        UpdateAsyncTask(NoteDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Note... notes) {
            for (Note note : notes) {
                this.dao.update(note);
            }
            return null;
        }
    }


    private class InsertAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO dao;

        InsertAsyncTask(NoteDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            for (Note note : params) {
                this.dao.insertNotes(note);
            }
            return null;
        }
    }

    public void setUpMap() {
        // myMap.setOnMapClickListener(this);

        /*

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this, null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        */
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.myMap = googleMap;
        LatLng location = new LatLng(47.643062, 6.839540); // A changer, mettre location telephone


        MarkerOptions options=new MarkerOptions().snippet(getString(R.string.snippet))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .anchor(0.5f, 0.5f);
        options.position(location);
        myMap.addMarker(options);
        myMap.moveCamera(CameraUpdateFactory.newLatLng(location));

        /*
        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
        */
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onMapClick(LatLng point) {

        MarkerOptions options=new MarkerOptions().snippet(getString(R.string.snippet))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .anchor(0.5f, 0.5f);
        options.position(point);
        myMap.addMarker(options);
    }



    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         *
         */
     /*   if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }*/
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode,
    *//*                                       @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();*//*
    }*/

    private void updateLocationUI() {
      /*  if (myMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                myMap.setMyLocationEnabled(true);
                myMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                myMap.setMyLocationEnabled(false);
                myMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }*/
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */

        /*try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            myMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }*/
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.option_get_place) {
            showCurrentPlace();
        }
        return true;
    }*/


}
