package com.example.notit.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.notit.R;

public class NoteViewHolder extends RecyclerView.ViewHolder {

    public TextView titreView, dateView, descriptionView;

    public NoteViewHolder(View itemView) {
        super(itemView);
        titreView = (TextView) itemView.findViewById(R.id.text_view_note_titre);
        dateView = (TextView) itemView.findViewById(R.id.text_view_note_date);
        descriptionView = (TextView) itemView.findViewById(R.id.text_view_note_description);
    }


}
