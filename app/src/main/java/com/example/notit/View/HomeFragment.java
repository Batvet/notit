package com.example.notit.View;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.notit.Accessor.AppDatabase;
import com.example.notit.Model.Note;
import com.example.notit.Accessor.NoteDAO;
import com.example.notit.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private List<Note> noteList = new ArrayList<>();
    private NoteListAdapter adapter;
    private RecyclerView recyclerView;

    private AppDatabase appdb;
    private NoteDAO noteDAO;

    private View view;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.note_recycler_view);

        appdb = AppDatabase.getDatabase(getContext());
        noteDAO = appdb.noteDAO();


        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (! sp.getBoolean(getResources().getString(R.string.key_is_db_initialized),false)) {
            prepareNoteData();
            sp.edit().putBoolean(getResources().getString(R.string.key_is_db_initialized),true).commit();
        }

        (new GetAllNotesAsyncTask(noteDAO)).execute();

        setUpRecyclerView();
        setUpItemTouchHelper();

        return view;
    }

    public void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
    }

    //Delete Note onswiped
    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int deletedIndex = viewHolder.getAdapterPosition();
                final Note deletedItem = noteList.get(viewHolder.getAdapterPosition());
                adapter.removeItem(deletedIndex);
                (new DeleteAsyncTask(noteDAO)).execute(deletedItem);

                Snackbar snackbar = Snackbar.make(view, "Note deleted", Snackbar.LENGTH_LONG)
                        .setAction("CANCEL", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // undo is selected, restore the deleted item
                                adapter.restoreItem(deletedItem, deletedIndex);
                                (new InsertAsyncTask(noteDAO)).execute(deletedItem);
                            }
                        });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void prepareNoteData() {
        String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

        Note n1 = new Note("No note", "Pas de note comme description", date, 47.6387143f, 6.8370225f);
        Note n2 = new Note("TO DO : Today", "A faire c\'est important", date, 47.6387143f, 6.8370225f);
        Note n3 = new Note("Courses pour demain", "Bravo \n ça \n marche \n fesdfsd \n fshgez \n fsefs", date, 47.6387143f, 6.8370225f);
        Note n4 = new Note("Apprendre l'anglais", "Ceci est un texte", date, 47.6387143f, 6.8370225f);
        Note n5 = new Note("Oral d'expression", "YAASSSS", date, 47.6387143f, 6.8370225f);
        Note n6 = new Note("Tester le dernier jeu", "It\'s rewind time", date, 47.6387143f, 6.8370225f);
        Note n7 = new Note("Ecrire le liver", "Un texte est une série orale ou écrite " +
                "de mots perçus comme constituant un ensemble cohérent, porteur de sens et utilisant " +
                "les structures propres à une langue (conjugaisons, construction et association des " +
                "phrases…)1. Un texte n'a pas de longueur déterminée sauf dans le cas de poèmes à " +
                "forme fixe comme le sonnet ou le haïku.", date, 47.6387143f, 6.8370225f);
        Note n8 = new Note("Faire du sport", "Le sens figuré d'éléments de langage organisés et enchaînés apparaît avant l\'Empire romain : il désigne un agencement particulier du discours.", date, 47.6387143f, 6.8370225f);
        Note n9 = new Note("Aller à Paris", "Un texte répond de façon plus ou moins pertinente à des critères qui en déterminent la qualité littéraire", date, 47.6387143f, 6.8370225f);
        Note n10 = new Note("Commande pizzas 6 personnes", "Peperonni canibale hawainne", date, 47.6387143f, 6.8370225f);
        Note n11 = new Note("NE PAS OUBLIER", "ATTENTION rendre avant hier et faire le cake", date, 47.6387143f, 6.8370225f);
        Note n12 = new Note("Jeu société pour ce weekend", "Ici tu réussit pélo", date, 47.6387143f, 6.8370225f);
        Note n13 = new Note("Gladiateur & compet", "JE SUIS UN F*CKING GLAD :D", date, 47.6387143f, 6.8370225f);

        (new InsertAsyncTask(noteDAO)).execute(n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    private class InsertAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO dao;

        InsertAsyncTask(NoteDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            for (Note note : params) {
                this.dao.insertNotes(note);
            }
            return null;
        }
    }

    private  class DeleteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO dao;

        public DeleteAsyncTask(NoteDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            for (Note note : notes) {
                this.dao.deleteNotes(note);
            }
            return null;
        }
    }

    private class GetAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDAO mAsyncTaskDao;
        ArrayList<Note> notes;

        public GetAllNotesAsyncTask(NoteDAO noteDAO) {
            this.mAsyncTaskDao = noteDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            notes = new ArrayList<>(mAsyncTaskDao.getAllNotes());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            noteList = notes;
            adapter = new NoteListAdapter(noteList);
            recyclerView.setAdapter(adapter);
        }
    }
}
